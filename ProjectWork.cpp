﻿#include <iostream> 
#include <time.h>
#include "Helper.h"

// Создаем функцию нахождения всех четных чисел массива
void FindNumber(bool isBool)
{
    int ArrayNumbers[] = { 2, 66, 435, 34, 7, 97, 542, 765, 5, 1243, 312 };
    int ArrayLength = sizeof(ArrayNumbers) / sizeof(ArrayNumbers[0]); // Длина массива

    for (int i = 0; i < ArrayLength; i++)     
    {
        //Если элемент массива по проверке битам является подходит под четность/нечетность, то выводится в консоль
        ((ArrayNumbers[i] & isBool) == 0) && std::cout << ArrayNumbers[i] << "\n";
    }
}

void GetDobleArray()
{
    //const int FirstSize = 2;
    const int Size = 5;
    int DoubleArray[Size][Size] = { {0, 1, 2, 3, 4}, {1, 2, 3, 4, 5}, {2, 3, 4, 5, 6}, {3, 4, 5, 6, 7}, {4, 5, 6, 7, 8} };

    for (int i = 0; i < Size; i++)
    {
        for (int j = 0; j < Size; j++)
        {
            std::cout << DoubleArray[i][j];
        }
        std::cout << "\n";
    }

    // Получаем дату
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    //Вычисляем строку массива и находим её сумму
    int GetLine = buf.tm_wday % Size;
    int Summa = 0;
    for (int l = 0; l < Size; l++) 
    {
        Summa = Summa + DoubleArray[GetLine][l];
    }

    std::cout << "The sum of all the items in the row - " << Summa << "\n";
}

//Создаем класс вектор
class Vector
{
//Приватные кооординаты
private :
    double x;
    double y;
    double z;
//Задаём изначальные координаты вектора, и из будуще заданных конечных координат узнаем длину
public :
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << "\n" << x << ' ' << y << ' ' << z;
    }

};

int main()
{
    //std::string Revolver = "Valera nastalo tvoye vremya!";

    //std::cout << Revolver << "\n";
    //std::cout << "Count of symbols used " << Revolver.length() << "\n";
    //std::cout << "Starts with " << "\"" << (Revolver.at(0)) << "\"" << " and ends with " << "\"" << Revolver.back() << "\"" << "\n";

    //std::cout << "Result = " << SquareSumm(3, 4) << std::endl;

    //FindNumber(true);

   //GetDobleArray();

    Vector v(15, 15, 15);
    v.Show();

    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
